package cz.vsb.vea.cz.vsb.vea.lab02v2.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.RequestScope;

import cz.vsb.vea.cz.vsb.vea.lab02v2.models.Person;

@Service
public class PersonService {

	private List<Person> persons;

	
	public PersonService() {
		persons = new ArrayList<>();
		persons.add(new Person("aa", "bb", LocalDate.of(2020, 1, 1)));
		persons.add(new Person("cc", "dd", LocalDate.of(2019, 1, 1)));
		persons.add(new Person("ee", "ff", LocalDate.of(2020, 12, 12)));
		persons.add(new Person("gg", "hh", LocalDate.of(2019, 12, 12)));
	}
	
	public void addPerson(Person person){
		persons.add(person);
	}
	
	@Transactional
	public List<Person> getPersons() {
		return persons;
	}
	
	
}
